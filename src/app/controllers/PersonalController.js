
const Course = require('../models/Course');
const { multipleMongooseToObj, mongooseToObj } = require('../../util/mongoose');


class PersonalController {

    async storedCourses(req, res, next) {

        try {
            let courseQuery = Course.find({});

            let promise = await Promise.all([
                Course.countDocumentsDeleted(),
                courseQuery.sortable(req)
            ]);

            let resultArray = promise;

            res.render('personal/stored-courses', {
                deletedCount: resultArray[0],
                courses: multipleMongooseToObj(resultArray[1]),
            });
        } catch (error) {
            next(error);
        }
    }

    trashed (req, res, next) {
        
        Course.findDeleted({}).then((deletedCourses) =>
            res.render('personal/trashed-courses', {
                deletedCourses: multipleMongooseToObj(deletedCourses),
            }),
        ).catch(next);
        
    }

}

module.exports = new PersonalController;