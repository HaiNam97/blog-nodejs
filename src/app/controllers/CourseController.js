
const Course = require('../models/Course');
const { multipleMongooseToObj, mongooseToObj } = require('../../util/mongoose');


class CourseController {

    async show(req, res, next) {

        try {
            let course = await Course.findOne({slug: req.params.slug}); 
            res.render('courses/show', {
                course: mongooseToObj(course)
            });
        } catch (error) {
            next(error);
        }    
        
    }

    create(req, res, next) {
        res.render('courses/create');
    }

    store(req, res, next) {
        
        const formData = req.body;
        formData.thumbnail = `https://img.youtube.com/vi/${formData.video_id}/sddefault.jpg`;
        const course = new Course(formData);
        
        course.save()
            .then(() => res.redirect('/')).catch(next);
    }

    async edit(req, res, next) {
        let course = await Course.findById(req.params.id);
        try {
            res.render('courses/edit', {
                course: mongooseToObj(course)
            });
        } catch (error) {
            next(error);
        }
    }

    async update (req, res, next) {
        
        try {
            let update = await Course.updateOne({
                _id: req.params.id 
            }, req.body); 

            if (update) {
                res.redirect('/personal/courses');
            }
        } catch (error) {
            next(error);
        }    

    }

    async delete (req, res, next) {

        try {
            let remove = await Course.delete({
                _id: req.params.id
            });

            if (remove) {
                res.redirect('back');
            }
        } catch (error) {
            next(error);
        }

    }

    async forceDestroy (req, res, next) {
        try {
            let destroy = await Course.deleteOne({
                _id: req.params.id
            });
            if (destroy) {
                res.redirect('back');
            }
        } catch (e) {
            next(e);
        }
    }

    async restore (req, res, next) {
        try {
            let restore = await Course.restore({
                _id: req.params.id
            }); 

            if (restore) {
                res.redirect('back');
            }
        } catch (error) {
            next(error);
        }
    }

    async handleFormAction (req, res, next) {
        try {
            switch(req.body.action) {
                case 'delete':

                    let softDelete = await Course.delete({
                        _id: {$in: req.body.courseIds}
                    });

                    if (softDelete.matchedCount) {
                        res.redirect('back');
                    }

                    break;
                default:
                    res.json({message: 'Action is invalid'});
            }
        } catch (error) {
            next(error);
        }
    }

}

module.exports = new CourseController;