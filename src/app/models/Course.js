const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const mongooseDelete = require('mongoose-delete');
const autoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const Course = new Schema({
    id: {type: Number},
    name: { type: String, required: true },
    description: { type: String},
    thumbnail: { type: String },
    video_id: {type: String, required: true},
    level: {type: String},
    slug: { type: String, slug: "name", index: {unique: true} }
}, {
    _id: false,
    timestamps: true 
});

Course.query.sortable = function (req) {
    let targetColumn = req.query.column;
    let sortType = req.query.type;

    if (req.query.hasOwnProperty('_sort')) {
     const isValidType = ['asc', 'desc'].includes(sortType);
     
     let sortTypeLast = isValidType ? sortType : 'desc'
     
     return this.sort({
        [targetColumn]: sortTypeLast
     });   
    }    
    return this;
};

mongoose.plugin(slug);
Course.plugin(autoIncrement);
Course.plugin(mongooseDelete, {
    deletedAt: true,
    overrideMethods: 'all'
});

module.exports = mongoose.model('Course', Course);