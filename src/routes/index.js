
const newsRouter = require('./news');
const siteRouter = require('./site');
const coursesRouter = require('./courses');
const personalRouter = require('./personal');

function route(app) {

      app.use('/news', newsRouter);

      app.use('/personal', personalRouter);

      app.use('/courses', coursesRouter);
      
      
      app.use('/', siteRouter);
      
}

module.exports = route;