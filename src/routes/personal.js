const express = require('express');
const router = express.Router();

const personalController = require('../app/controllers/PersonalController');

router.get('/courses', personalController.storedCourses);
router.get('/courses/trashed', personalController.trashed);

module.exports = router;