
const handlebars = require('handlebars');

module.exports = {
    sum: (a, b) => a + b,
    sortable: (field, sort) => {

      const sortType = field === sort.column ? sort.type : 'default';

      const icon = {
        default: 'oi oi-elevator',
        asc: 'oi oi-sort-ascending',
        desc: 'oi oi-sort-descending'
      };

      const type = {
        default: 'desc',
        asc: 'desc',
        desc: 'asc'
      };

      const sortTypeValue = type[sortType];

      const iconClass = icon[sortType];

      const address = handlebars.escapeExpression(`?_sort&column=${field}&type=${sortTypeValue}`);

      const output =  `
        <a href="${address}">
          <span class="${iconClass}"></span>
        </a>
      `;

      return new handlebars.SafeString(output);
    }
};