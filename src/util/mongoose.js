module.exports = {
    multipleMongooseToObj: function (mongooseArrays) {
        return mongooseArrays.map(item => item.toObject());
    },

    mongooseToObj: function (mongoose) {
        return mongoose ? mongoose.toObject() : mongoose;
    }
};